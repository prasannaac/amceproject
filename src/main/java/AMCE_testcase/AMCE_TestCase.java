package AMCE_testcase;

import org.testng.annotations.Test;

import Pages.LoginPage;
import amce.AMCE_base;

public class AMCE_TestCase extends AMCE_base {

	
	@Test
	public void TC001() {
		
		new LoginPage()
		.enterEmail()
		.enterPassword()
		.clickLoginButton()
		.clickVendorTab()
		.clickSFVendor()
		.enterTaxID()
		.clickSearchButton()
		.getVendorName();
		
	}
}
