package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import amce.AMCE_base;

public class SearchPage extends AMCE_base {

	public SearchPage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.ID, using = "vendorTaxID") WebElement eleTaxID ;
	@FindBy(how = How.ID, using ="buttonSearch") WebElement eleButtonSearch;
	
	public SearchPage enterTaxID() {
		eleTaxID.sendKeys("RO094782");
		return this;
	}
	
	public SearchResultsPage clickSearchButton() {
		eleButtonSearch.click();
		return new SearchResultsPage();
		
	}
}
