package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import amce.AMCE_base;

public class SearchResultsPage extends AMCE_base {

	public SearchResultsPage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.XPATH, using = "//table//tr[2]/td[1]") WebElement vendorName ;

	public SearchResultsPage getVendorName() {
		System.out.println(vendorName.getText());
		return this;
	}

}
