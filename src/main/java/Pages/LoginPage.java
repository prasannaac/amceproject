package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import amce.AMCE_base;

public class LoginPage extends AMCE_base {

	public LoginPage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.ID, using = "email") WebElement eleEmail;
	@FindBy(how = How.ID, using = "password") WebElement elePassword;
	@FindBy(how = How.ID, using = "buttonLogin") WebElement eleclickLogin;
	
	
	public LoginPage enterEmail() {
		eleEmail.clear();
		eleEmail.sendKeys("acprasannaraj@gmail.com");
		return this;
	}
	
	public LoginPage enterPassword() {
		elePassword.clear();
		elePassword.sendKeys("Tommy@2");
		return this;
	}

	public DashboardPage clickLoginButton() {
		eleclickLogin.click();
		return new DashboardPage();
	}
	
}
