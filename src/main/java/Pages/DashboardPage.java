package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import amce.AMCE_base;

public class DashboardPage extends AMCE_base {
	public static Actions builder;
	
	public DashboardPage() {
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.XPATH, using = "//button[text() = ' Vendors']") WebElement eleVendortab ;
	@FindBy(how = How.XPATH, using = "//*[text()='Search for Vendor']") WebElement eleSFVendor;
	
		public DashboardPage clickVendorTab() {
			builder = new Actions(driver);
		builder.click(eleVendortab).perform();
		return this;
	}
	
	public SearchPage clickSFVendor() {
		builder = new Actions(driver);
		builder.click(eleSFVendor).perform();
		return new SearchPage();
	}
	
}
